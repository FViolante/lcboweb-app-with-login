package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {
	
	//Test Format
	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ramses123") );
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("r12345") );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("1ramses") );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("@ramses1") );
	}
	
	//Test Length
	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ramses123") );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ramses") );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("ramse") );
	}
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("ram") );
	}
	

}
