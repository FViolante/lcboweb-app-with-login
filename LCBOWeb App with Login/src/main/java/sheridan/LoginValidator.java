package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if(Character.isDigit(loginName.charAt(0))) {
			return false;
		}
		
		for(char c : loginName.toCharArray()) {
			if(!Character.isLetterOrDigit(c)) {
				return false;
			}
		}
		
		return loginName.length( ) >= 6 ;
	}
}
